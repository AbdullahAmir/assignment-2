const express = require('express');
const _ = require('lodash');
const models = require('../models');

const router = express.Router();

// Index
router.get('/', (req, res) => {
  models.Notebook.findAll({ order: [['createdAt', 'DESC']] })
    .then(notebooks => res.json(notebooks))
    .catch(err => res.status(500).json({ error: err.message }));
});

/* *** TODO: Fill in the API endpoints for notebooks *** */
router.post('/', (req, res) => {
  // Generate new posts in the database
  models.Notebook.create(postFilter(req.body))
    .then(notebook => res.json(notebook))
    .catch(err => res.status(422).json({ error: err.message }));
  });
 
  router.get('/:notebookId/notes', (req, res) => {
  //  Returning records to the database
   models.Note.findAll({where: {notebookId: req.params.notebookId}})
    .then(notes => res.json(notes))
    .catch(err => res.status(500).json({ error: err.message }));
  });
  
  // Destroy
  router.delete('/:notebookId', (req, res) => {
  // Delete the posts from database
    models.Notebook.destroy({ where: { id: req.params.notebookId } })
      .then(() => res.json({}))
      .catch(err => res.status(500).json({ error: err.message }));
  });
  // Updating the posts in the database
  
  router.put('/:notebookId',(req, res) => {
    models.Notebook.findById(req.params.notebookId)
    .then(notebook => notebook.update(postFilter(req.body)))
    .then(notebook => res.json(notebook))
  
  });

module.exports = router;
