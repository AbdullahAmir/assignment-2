const index = require('../controllers/index');
const notebooks = require('../controllers/notebooks');
const notes = require('../controllers/notes');

const routes = {};

// Connecting the controllers to specific base paths.

routes.connect = (app) => {
  // Using the index controller for /
  app.use('/', index);
  app.use('/notebooks', notebooks);
  app.use('/notes', notes);
};

module.exports = routes;
